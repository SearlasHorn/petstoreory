package models

type ShipDate string

type OrderStatus string

const (
	StatusOrderPlaced    OrderStatus = "placed"
	StatusOrderApproved  OrderStatus = "approved"
	StatusOrderDelivered OrderStatus = "delivered"
)

type Order struct {
	Id       int64       `json:"order_id"`
	PetId    int64       `json:"petId"`
	Quantity int32       `json:"quantity"`
	ShipDate ShipDate    `json:"ship_date"`
	Status   OrderStatus `json:"status"`
	Complete bool        `json:"complete"`
}
