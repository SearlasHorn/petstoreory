package models

type StatusPet string

const (
	StatusPetAvailable StatusPet = "available"
	StatusPetPending   StatusPet = "pending"
	StatusPetSold      StatusPet = "sold"
)

type Pet struct {
	Id       interface{} `json:"id"`
	Category Category    `json:"category"`
	Name     string      `json:"name"`
	//PhotoUrls PhotoUrls `json:"photo_urls"`
	Tags   []Tag     `json:"tags"`
	Status StatusPet `json:"status"`
}

type Tag struct {
	Name string `json:"name"`
}

type Category struct {
	Id   int64
	Name string
}
