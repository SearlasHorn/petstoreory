package utils

import (
	"petstoreory/api/models"
)

var (
	User = models.User{
		Username:   "JohnDoe",
		FirstName:  "John",
		LastName:   "Doe",
		Email:      "hornholdt@gmail.com",
		Password:   "pass11",
		Phone:      "+380666666666",
		UserStatus: apiModels.ActiveUserStatus,
	}

	PetCategory = models.Category{
		Name: "Prime Category",
	}

	PetTag = models.Tag{
		Name: "teg №1",
	}

	PetTag2 = models.Tag{
		Name: "teg №2",
	}

	PetTag3 = models.Tag{
		Name: "teg №3",
	}

	Pet = models.Pet{
		Id:       1,
		Category: PetCategory,
		Name:     "Jo",
		//PhotoUrls: Photo
		Tags:   []models.Tag{PetTag, PetTag2, PetTag3},
		Status: models.StatusPetAvailable,
	}

	Pet2 = models.Pet{
		Id:       2,
		Category: PetCategory,
		Name:     "Bo",
		//PhotoUrls: Photo
		Tags:   []models.Tag{PetTag, PetTag2, PetTag3},
		Status: models.StatusPetAvailable,
	}

	Pet3 = models.Pet{
		Id:       3,
		Category: PetCategory,
		Name:     "Pico",
		//PhotoUrls: Photo
		Tags:   []models.Tag{PetTag, PetTag2, PetTag3},
		Status: models.StatusPetAvailable,
	}

	InventoryByStatus = map[string]int{
		"test status1": 1,
		"test status2": 2,
		"test status3": 3,
		"test status4": 4,
	}

	Order = models.Order{
		Id:       1,
		PetId:    1,
		Quantity: 1,
		ShipDate: "13-02-2020",
		Status: models.StatusOrderPlaced,
	}
)
